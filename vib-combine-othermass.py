#!/usr/bin/env python3
#ATTENTION constrained atoms MUST be at end of the POSCAR
#OUTCARs have to be renamed starting from OUTCAR1 in the same order of fragmentation
import os
import sys
import numpy as np
import pickle
import glob
import time
#import ase.vibrations.vibrations as vib
from math import sin, pi, sqrt, log
from ase.utils import basestring
import ase.units as units
from ase.io import write, read
from ase.parallel import rank, paropen
#from sys import argv

start = time. time()

def read_forces(atoms, all=False):
    """Method that reads forces from OUTCAR file.

    If 'all' is switched on, the forces for all ionic steps
    in the OUTCAR file be returned, in other case only the
    forces for the last ionic configuration is returned."""

    file = open(filename, 'r')
    lines = file.readlines()
    file.close()
    n = 0
    if all:
        all_forces = []
    for line in lines:
        if line.rfind('TOTAL-FORCE') > -1:
            forces = []
            for i in range(len(atoms)):
                forces.append(np.array([float(f) for f in
                                        lines[n + 2 + i].split()[3:6]]))
            if all:
                all_forces.append(np.array(forces))
        n += 1
    if all:
        return np.array(all_forces)
    else:
        return np.array(forces)

def read_potim():
    potim = None
    for line in open(filename):
        if line.find('POTIM') != -1:
            potim = float(line.split('=')[1].split()[0].strip())
    return potim            

#def readpc():#constrained atoms MUST be at end of the POSCAR
    #c=int(argv[1])
    #delta=read_potim()
    #atoms=read(filename)
    #l=len(atoms[:-c])
    ##all_forces=read_forces(atoms,all)
    ##forces=glob.glob('*.pckl')#all_forces[1:len(all_forces)]
    ##n = len(forces)/2)
    ##natoms=int(n/3)
    #rang=range(l)
    #indices=np.asarray(rang)
    ##print(indices)

    #H = np.zeros((3*l,3*l))
    ##print(np.shape(H))
    ##print("doing readpc")
    #r = 0
    #for a in range(0,6*l,2):
         #with open("force" + str(a+1)+".pckl", 'rb') as fm:
            #fminus = pickle.load(fm)[:-c]
            ##print(fminus)
         #with open("force" + str(a+2)+".pckl", 'rb') as fp:
            #fplus = pickle.load(fp)[:-c]
            ##print(fplus)
            ##print(np.shape(fplus))
         #H[r] = .5 * (fminus - fplus)[indices].ravel()
         #H[r] /= delta
         #r += 1
    ##print(H)    
    #H += H.copy().T#Symmetrization
    ##print(H)
    #m = atoms.get_masses()
    ##print(m)
    #im = np.repeat(m[indices]**-0.5, 3)
    ##print(im)
    
    #omega2, modes = np.linalg.eigh(im[:, None] * H * im)#mass scaled hessian
    ##print(omega2)
    #modes = modes.T.copy()#Unscaled
    ##print(modes)
    #modesc=modes*im#mass-scaled
    ##print(modesc)

    ## Conversion factor:
    #s = units._hbar * 1e10 / sqrt(2*units._e * units._amu)
    #hnu = s * (omega2*-1).astype(complex)**0.5#Swap sign of eigval. In eV
    #print(hnu)
    #invcm = hnu/units.invcm
    ##print(invcm)
    
def readc(log=sys.stdout):#Full Hessian
    delta=read_potim()
    atoms=read(filename)
    n = int(g/2)
    natoms=int(n/3)
    rang=range(natoms)
    indices=np.asarray(rang)
    
    #Hessian computation
    H = np.empty((n, n))
    r = 0
    for a in range(0,n*2,2):
         with open("force" + str(a+1)+".pckl", 'rb') as fm:
            fminus = pickle.load(fm)
            #print(fminus)
         with open("force" + str(a+2)+".pckl", 'rb') as fp:
            fplus = pickle.load(fp)
         H[r] = .5 * (fminus - fplus)[indices].ravel()
         H[r] /= (2*delta)
         r += 1 
    H += H.copy().T #Symmetrization
    m = atoms.get_masses()
    im = np.repeat((m[indices])**-0.5, 3)
    omega2, modes = np.linalg.eigh(im[:, None] * H * im)#mass scaled hessian
    #modes = modes.T.copy()#Unscaled
    #modesc=modes*im#mass-scaled

    # Conversion factor:
    s = units._hbar * 1e10 / sqrt(units._e * units._amu)
    hnu = s * (omega2*-1).astype(complex)**0.5#Swap sign of eigval. In eV
    #invcm = hnu/units.invcm
    
    #Write results to file
    if isinstance(log, basestring):
        log = paropen(log, 'a')
    write = log.write

    s = 1/units.invcm
    
    write('---------------------\n')
    write('  #    meV     cm^-1\n')
    write('---------------------\n')
    for n, e in enumerate(hnu):
        if e.imag != 0:
            c = 'i'
            e = e.imag
        else:
            c = ' '
            e = e.real
        write('%3d %6.1f%s  %7.1f%s\n' % (n+1, 1000 * e, c, s * e, c))
    write('---------------------\n')
    
          
g = 0 #global counter
files=len(glob.glob('OUTCAR*'))

#Iterate over OUTCARs
for x in range(files):
    filename=('OUTCAR' + str(x+1))
    atoms = read(filename)
    all_forces=read_forces(atoms,all)
    forces=all_forces[1:len(all_forces)]
    dof=len(forces)

    #Output forces to machine readeable format
    for i in range(dof):
        force=forces[i]
        pickle_out = paropen("force" + str(g+1)+".pckl","wb")
        pickle.dump(force, pickle_out)
        pickle_out.close()
        #test=open("force" + str(g+1)+".pckl","rb")
        #exa=pickle.load(test)
        #print(exa)
        g += 1

#Consider different usecases 
#if len(sys.argv) == 1:#If no argument is given, condstruct full Hessian
    #readc()
#else:
    #readpc()

readc(log='frequencies')
#summary()

#Cleanup
for f in glob.glob('*.pckl'):
    os.remove(f)
    
end = time. time()
print(end - start)

#TODO
#Arbitrary position of displaced atoms (read a (1,0) vector from POSCAR)
#parallelize (Over: OUTCARs)
#cpickle
