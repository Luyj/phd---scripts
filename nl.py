#!/usr/bin/env python3.4
import numpy as np
import matplotlib.pyplot as pl
import csv
from matplotlib.pyplot import hist as mplh
from ase.neighborlist import neighbor_list as nl
from ase.io import write, read
a=read('POSCAR')
#l=[1.3,0.5,0.5,0.5] #atom specific list
#d = nl('i', a, l)
d = nl('i', a, { # create list
    ('N', 'N'): 1.8826, 
    ('C', 'H'): 1.2, 
    ('C', 'C'): 1.89002, 
    ('C', 'N'): 1.79202
    })
#e = d.flatten()
print(len(d)/2) # number of bonds

h, bin_edges = np.histogram(d, bins=100) # histogram
pdf = h/(4*np.pi/3*(bin_edges[1:]**3 - bin_edges[:-1]**3))# * a.get_volume()/len(a)
#print(pdf)
fig = mplh(pdf, bins=100)
pl.savefig('histogram.png')

np.savetxt("array.txt",d,delimiter=' ',newline=' ')
#write('slab.xyz',a)

np.savetxt("histogram.txt",pdf,delimiter=' ',newline=' ')

#f = open("histogram.txt", "w")
#f.write(pdf)
