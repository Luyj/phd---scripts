import numpy as np
import matplotlib.pyplot as pl
import csv
from matplotlib.pyplot import hist as mplh
from ase.neighborlist import neighbor_list as nl
from ase.io import write, read
k = open("bonds-table.txt", "w+")
for i in range(2000):
    a=read('XDATCAR',index=i)
    d = nl('d', a, { # create bond list
        ('N', 'N'): 1.8826, 
        ('C', 'H'): 1.2, 
        ('N', 'H'): 1.2, 
        ('C', 'C'): 1.89002, 
        ('C', 'N'): 1.79202,
        ('S', 'O'): 1.8, 
        ('S', 'N'): 2.28849 
        })
    print(len(d)/2) # number of bonds
    k.write(str(i) + ' ' + str(len(d)/2) +"\n")

print("done")    
k.close()

#h, bin_edges = np.histogram(d, bins=100) # histogram
#pdf = h/(4*np.pi/3*(bin_edges[1:]**3 - bin_edges[:-1]**3))# * a.get_volume()/len(a)
#print(pdf)
#fig = mplh(pdf, bins=100)
#pl.savefig('histogram.png')
#np.savetxt("histogram.txt",pdf,delimiter=' ',newline=' ')

#np.savetxt("array.txt",e,delimiter=' ',newline=' ')
#write('slab.xyz',a)


#f = open("histogram.txt", "w")
#f.write(pdf)

#TODO
# performance??? parallel?
# automatic number of trajectory points see vef.py
    #filename = 'OUTCAR'
    #if not isfile(filename):
    #    print 'No such file: %s' % filename
    #    exit(1)
    #
    #traj = aselite.read_vasp_out(filename)
    #if len(traj) == 0:
    #    exit(0)
    #
# Full element-bond list from Gaussian?
