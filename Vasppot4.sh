#!/bin/sh

myPWD=`pwd`
POTDIR='/home/surfacechem/mr_soft/chemsoft/lx24-amd64/VASP/potpaw_PBE.54/'

if [ -f POSCAR ]
then
   rm POTCAR
   myAGREP=`head -1 POSCAR | tail -1`
   echo "POTCAR Version 4 für folgende Elemente (siehe Zeile 1): " $myAGREP
   for i in $myAGREP
   do
      cat $POTDIR$i/POTCAR >> $myPWD/POTCAR
   done
else
   echo "Hier gibts kein POSCAR!"
fi

