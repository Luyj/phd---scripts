#WARNING: Do not use with constrained atoms. DOFs will be miscounted.
#WARNING: When calculating TS reduce DOF manually by 1. I.e. dof=3*natoms-6.
import os
import sys
import numpy as np
from ase.io import write, read
from ase.calculators.vasp import vasp
from ase.thermochemistry import IdealGasThermo

#sys.stdout = open('thermo.txt', 'w') #output to file instead

atoms = read('OUTCAR')
natoms=len(atoms)
print(atoms)
dof=3*natoms-5 #Degrees of freedom
print(dof)
#potentialenergy = atoms.get_potential_energy()
potentialenergy = vasp.Vasp.read_energy('OUTCAR',all)[1][0] #returns the last entry
print(potentialenergy)

energies = vasp.Vasp.read_vib_freq('OUTCAR')[0] # only real frequencies
#print(energies)
vib_energies = [i / 1000 for i in energies][0:dof] #scaling to eV and removing lowest DOF
print(vib_energies)


thermo = IdealGasThermo(vib_energies=vib_energies,
                        potentialenergy=potentialenergy,
                        atoms=atoms,
                        geometry='linear', # 'nonlinear', 'monoatomic'
                        symmetrynumber=2, # DONT FORGET TO SET THE RIGHT SYMMETRY NUMBER! E.g. Acetylene=2. CO=1
                        spin=0, # S/2
                        natoms=natoms) 
G = thermo.get_gibbs_energy(temperature=300, pressure=100000.)
