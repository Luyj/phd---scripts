import os
import sys
import numpy as np
from ase.io import write, read
from ase.calculators.vasp import vasp
from ase.thermochemistry import HinderedThermo

#sys.stdout = open('thermo.txt', 'w') #output to file instead

atoms = read('OUTCAR')
potentialenergy = vasp.Vasp.read_energy('OUTCAR',all)[1][0] #returns the last entry
print(potentialenergy)
energies = vasp.Vasp.read_vib_freq('OUTCAR')[0] # only real frequencies
#print(energies)
nfreq=int(len(energies)/2-3) #remove 1 frustrated rotation, 2 frustrated translations. CHECK AND MODIFY BY HAND IF NECESSARY!
#print(nfreq)
vib_energies = [i / 1000 for i in energies][0:nfreq] #scaling to eV and removing doubly counted frequencies
print(vib_energies)

trans_barrier_energy=0.049313 # in eV
rot_barrier_energy=0.017675 # in eV
sitedensity=1.5e15 #density of surface sites in cm^-2
rotationalminima= 6# number of equivalent minima for an adsorbateâs full rotation. For example, 6 for an adsorbate on an fcc(111) top site
symmetrynumber=1, # symmetry number of the adsorbate.

thermo = HinderedThermo(vib_energies=vib_energies,
                        trans_barrier_energy=trans_barrier_energy,
                        rot_barrier_energy=rot_barrier_energy,
                        sitedensity=sitedensity,
                        rotationalminima=rotationalminima,
                        potentialenergy=potentialenergy,
                        symmetrynumber=symmetrynumber,
                        atoms=atoms
                        )

F = thermo.get_helmholtz_energy(temperature=300)
