import os
import sys
import numpy as np
from ase.io import write, read
from ase.calculators.vasp import vasp
from ase.thermochemistry import HarmonicThermo

#sys.stdout = open('thermo.txt', 'w') #output to file instead

potentialenergy = vasp.Vasp.read_energy('OUTCAR',all)[1][0] #returns the last entry
print(potentialenergy)
energies = vasp.Vasp.read_vib_freq('OUTCAR')[0] # only real frequencies
#print(energies)
nfreq=int(len(energies)/2)
#print(nfreq)
vib_energies = [i / 1000 for i in energies][0:nfreq] #scaling to eV and removing doubly counted frequencies
print(vib_energies)


thermo = HarmonicThermo(vib_energies=vib_energies,
                        potentialenergy=potentialenergy
                        ) 
F = thermo.get_helmholtz_energy(temperature=300)
