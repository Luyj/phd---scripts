#!/usr/bin/env python3
#ATTENTION constrained atoms MUST be at end of the POSCAR
import os
import sys
import numpy as np
import pickle
import ase.units as units
#import ase.atoms as atoms
#import ase.calculators.vasp.vasp as vasp
import ase.vibrations.vibrations as vib
from math import sin, pi, sqrt, log
from ase.io import write, read
from decimal import *

#Wites out force-vecors in full length. Can calculate partial Hessian.

def read_forces(atoms, all=False):
    """Method that reads forces from OUTCAR file.

    If 'all' is switched on, the forces for all ionic steps
    in the OUTCAR file be returned, in other case only the
    forces for the last ionic configuration is returned."""

    file = open('OUTCAR', 'r')
    lines = file.readlines()
    file.close()
    n = 0
    if all:
        all_forces = []
    for line in lines:
        if line.rfind('TOTAL-FORCE') > -1:
            forces = []
            for i in range(len(atoms)):
                forces.append(np.array([float(f) for f in
                                        lines[n + 2 + i].split()[3:6]]))
            if all:
                all_forces.append(np.array(forces))
        n += 1
    if all:
        return np.array(all_forces)
    else:
        return np.array(forces)

def read_potim():
        potim = None
        for line in open('OUTCAR'):
            if line.find('POTIM') != -1:
                potim = float(line.split('=')[1].split()[0].strip())
        return potim            

def readf():
        delta=read_potim()
        atoms=read('OUTCAR')
        l=len(atoms)
        all_forces=read_forces(atoms,all)
        forces=all_forces[1:len(all_forces)]
        n = int(len(forces)/2)
        #natoms=int(n/3)
        rang=range(l)
        indices=np.asarray(rang)
        #print(indices)

       
        H = np.zeros((3*l, 3*l))
        r = 0
        for a in range(0,n*2,2):
             with open("force" + str(a+1)+".pckl", 'rb') as fm:
                fminus = pickle.load(fm)
             with open("force" + str(a+2)+".pckl", 'rb') as fp:
                fplus = pickle.load(fp)
             H[r] = .5 * (fminus - fplus)[indices].ravel()
             H[r] /= delta
             r += 1  
        H += H.copy().T #Symmetrization
        
        m = atoms.get_masses()
        im = np.repeat(m[indices]**-0.5, 3)
        omega2, modes = np.linalg.eigh(im[:, None] * H * im)#mass scaled hessian
        modes = modes.T.copy()#Unscaled
        modesc=modes*im#mass-scaled

        # Conversion factor:
        s = units._hbar * 1e10 / sqrt(2*units._e * units._amu)
        hnu = s * (omega2*-1).astype(complex)**0.5#Swap sign of eigval. In eV
        invcm = hnu/units.invcm
        print(invcm[0:n])

atoms = read('OUTCAR')
all_forces=read_forces(atoms,all)
forces=all_forces[1:len(all_forces)]
#print(forces)

dof=len(forces)

for i in range(dof):
 force=forces[i]
# print(force)

 pickle_out = open("force" + str(i+1)+".pckl","wb")
 pickle.dump(force, pickle_out)
 pickle_out.close()
 
#readf()

for i in range(dof):
 os.remove("force" + str(i+1)+".pckl")
 
##Cleanup
#for f in glob.glob('*.pckl'):
    #os.remove(f)

#TODO
