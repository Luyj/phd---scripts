#!/usr/bin/env python3
#ATTENTION constrained atoms MUST be at end of the POSCAR
#OUTCARs have to be renamed starting from OUTCAR1 in the same order of fragmentation
import os
import sys
import numpy as np
import pickle
import glob
import time
#import ase.vibrations.vibrations as vib
from math import sin, pi, sqrt, log
from ase.utils import basestring
import ase.units as units
from ase.io import write, read
from ase.parallel import rank, paropen
#from sys import argv

#Atomic masses taken from the POTCAR. Might be subject to intellectual property regulations!
#Rn is given wrong!
atomic_masses_54 = np.array([
    1.000,     #H
    4.000,     #He
    7.010,     #Li
    9.013,     #Be
    10.811,    #B
    12.011,    #C
    14.001,    #N
    16.000,    #O
    18.998,    #F
    20.180,    #Ne
    22.990,    #Na
    24.305,    #Mg
    26.981,    #Al
    28.085,    #Si
    30.974,    #P
    32.066,    #S
    35.453,    #Cl
    39.949,    #Ar
    39.098,    #K
    40.078,    #Ca
    44.956,    #Sc
    47.880,    #Ti
    50.941,    #V
    51.996,    #Cr
    54.938,    #Mn
    55.847,    #Fe
    58.933,    #Co
    58.690,    #Ni
    63.546,    #Cu
    65.390,    #Zn
    69.723,    #Ga
    72.610,    #Ge
    74.922,    #As
    78.960,    #Se
    79.904,    #Br
    83.800,    #Kr
    85.468,    #Rb
    87.620,    #Sr
    88.906,    #Y
    91.224,    #Zr
    92.910,    #Nb
    95.940,    #Mo
    98.906,    #Tc
    101.070,   #Ru
    102.906,   #Rh
    106.420,   #Pd
    107.868,   #Ag
    112.411,   #Cd
    114.820,   #In
    118.710,   #Sn
    121.750,   #Sb
    127.600,   #Te
    126.904,   #I
    131.294,   #Xe
    132.900,   #Cs
    137.327,   #Ba
    138.900,   #La
    140.115,   #Ce
    140.907,   #Pr
    144.240,   #Nd
    146.915,   #Pm
    150.360,   #Sm
    151.965,   #Eu
    157.250,   #Gd
    158.925,   #Tb
    162.500,   #Dy
    164.930,   #Ho
    167.260,   #Er
    168.930,   #Tm
    173.040,   #Yb
    174.967,   #Lu
    178.490,   #Hf
    180.948,   #Ta
    183.850,   #W
    186.207,   #Re
    190.200,   #Os
    192.220,   #Ir
    195.080,   #Pt
    196.966,   #Au
    200.590,   #Hg
    204.380,   #Tl
    207.200,   #Pb
    208.980,   #Bi
    208.942,   #Po
    209.987,   #At
    22.017,    #Rn ATTENTION
    223.020,   #Fr
    226.025,   #Ra
    227.028,   #Ac
    232.039,   #Th
    231.036,   #Pa
    238.029,   #U
    237.048,   #Np
    244.064,   #Pu
    243.061,   #Am
    247.000,   #Cm
    np.nan,    #Bk
    247.000    #Cf
]) 

#start = time. time()

def read_forces(atoms, all=False):
    """Method that reads forces from OUTCAR file.

    If 'all' is switched on, the forces for all ionic steps
    in the OUTCAR file be returned, in other case only the
    forces for the last ionic configuration is returned."""

    file = open(filename, 'r')
    lines = file.readlines()
    file.close()
    n = 0
    if all:
        all_forces = []
    for line in lines:
        if line.rfind('TOTAL-FORCE') > -1:
            forces = []
            for i in range(len(atoms)):
                forces.append(np.array([float(f) for f in
                                        lines[n + 2 + i].split()[3:6]]))
            if all:
                all_forces.append(np.array(forces))
        n += 1
    if all:
        return np.array(all_forces)
    else:
        return np.array(forces)

def read_potim():
    potim = None
    for line in open(filename):
        if line.find('POTIM') != -1:
            potim = float(line.split('=')[1].split()[0].strip())
    return potim            

#def readpc():#constrained atoms MUST be at end of the POSCAR
    #c=int(argv[1])
    #delta=read_potim()
    #atoms=read(filename)
    #l=len(atoms[:-c])
    ##all_forces=read_forces(atoms,all)
    ##forces=glob.glob('*.pckl')#all_forces[1:len(all_forces)]
    ##n = len(forces)/2)
    ##natoms=int(n/3)
    #rang=range(l)
    #indices=np.asarray(rang)
    ##print(indices)

    #H = np.zeros((3*l,3*l))
    ##print(np.shape(H))
    ##print("doing readpc")
    #r = 0
    #for a in range(0,6*l,2):
         #with open("force" + str(a+1)+".pckl", 'rb') as fm:
            #fminus = pickle.load(fm)[:-c]
            ##print(fminus)
         #with open("force" + str(a+2)+".pckl", 'rb') as fp:
            #fplus = pickle.load(fp)[:-c]
            ##print(fplus)
            ##print(np.shape(fplus))
         #H[r] = .5 * (fminus - fplus)[indices].ravel()
         #H[r] /= delta
         #r += 1
    ##print(H)    
    #H += H.copy().T#Symmetrization
    ##print(H)
    #m = atoms.get_masses()
    ##print(m)
    #im = np.repeat(m[indices]**-0.5, 3)
    ##print(im)
    
    #omega2, modes = np.linalg.eigh(im[:, None] * H * im)#mass scaled hessian
    ##print(omega2)
    #modes = modes.T.copy()#Unscaled
    ##print(modes)
    #modesc=modes*im#mass-scaled
    ##print(modesc)

    ## Conversion factor:
    #s = units._hbar * 1e10 / sqrt(2*units._e * units._amu)
    #hnu = s * (omega2*-1).astype(complex)**0.5#Swap sign of eigval. In eV
    #print(hnu)
    #invcm = hnu/units.invcm
    ##print(invcm)
    
def readc(log=sys.stdout):#Full Hessian
    delta=read_potim()
    atoms=read(filename)
    n = int(g/2)
    natoms=int(n/3)
    rang=range(natoms)
    indices=np.asarray(rang)
    
    #Hessian computation
    H = np.empty((n, n))
    r = 0
    for a in range(0,n*2,2):
         with open("force" + str(a+1)+".pckl", 'rb') as fm:
            fminus = pickle.load(fm)
            #print(fminus)
         with open("force" + str(a+2)+".pckl", 'rb') as fp:
            fplus = pickle.load(fp)
         H[r] = .5 * (fminus - fplus)[indices].ravel()
         H[r] /= (2*delta)
         r += 1 
    H += H.copy().T #Symmetrization
    mx = atoms.get_atomic_numbers()
    m = np.array([])
    for i in mx:
        m=np.append(m, atomic_masses_54[i-1])
    #m = atoms.get_masses()#ASE standard (IUPAC)
    im = np.repeat((m[indices])**-0.5, 3)
    omega2, modes = np.linalg.eigh(im[:, None] * H * im)#mass scaled hessian
    #modes = modes.T.copy()#Unscaled
    #modesc=modes*im#mass-scaled

    # Conversion factor:
    s = units._hbar * 1e10 / sqrt(units._e * units._amu)
    hnu = s * (omega2*-1).astype(complex)**0.5#Swap sign of eigval. In eV
    #invcm = hnu/units.invcm
    
    #Write results to file
    if isinstance(log, basestring):
        log = paropen(log, 'a')
    write = log.write

    s = 1/units.invcm
    
    write('---------------------\n')
    write('  #    meV     cm^-1\n')
    write('---------------------\n')
    for n, e in enumerate(hnu):
        if e.imag != 0:
            c = 'i'
            e = e.imag
        else:
            c = ' '
            e = e.real
        write('%3d %6.1f%s  %7.1f%s\n' % (n+1, 1000 * e, c, s * e, c))
    write('---------------------\n')
    
          
g = 0 #global counter
files=len(glob.glob('OUTCAR*'))

#Iterate over OUTCARs
for x in range(files):
    filename=('OUTCAR' + str(x+1))
    atoms = read(filename)
    all_forces=read_forces(atoms,all)
    forces=all_forces[1:len(all_forces)]
    dof=len(forces)

    #Output forces to machine readeable format
    for i in range(dof):
        force=forces[i]
        pickle_out = paropen("force" + str(g+1)+".pckl","wb")
        pickle.dump(force, pickle_out)
        pickle_out.close()
        #test=open("force" + str(g+1)+".pckl","rb")
        #exa=pickle.load(test)
        #print(exa)
        g += 1

#Consider different usecases 
#if len(sys.argv) == 1:#If no argument is given, condstruct full Hessian
    #readc()
#else:
    #readpc()

readc(log='frequencies')
#summary()

#Cleanup
for f in glob.glob('*.pckl'):
    os.remove(f)
    
#end = time. time()
#print(end - start)

#TODO
#Arbitrary position of displaced atoms (read a (1,0) vector from POSCAR)
#parallelize (Over: OUTCARs)
#cpickle
