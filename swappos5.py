#!/usr/bin/env python
#For POSCAR v5
import sys
arg3=sys.argv[1]
arg1=int(sys.argv[2])+8
arg2=int(sys.argv[3])+8

with open(arg3, 'r') as infile:
    lines = infile.readlines()

# swap lines
lines[arg1], lines[arg2] = lines[arg2], lines[arg1]

with open(sys.argv[1], 'w') as outfile:
    outfile.writelines(lines)
